/* 
 * Copyright (C) 2001-2011 Jacek Sieka, arnetheduck on gmail point com
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define BETA 0
#define HAS_RDEX 0

#define APP_AUTH_URL "https://svn.apexdc.net/auth/login_client/$WCMODS??local=1:$"
#define APP_AUTH_KEY TIGER(SETTING(CLIENTAUTH_USER) + "." + TIGER(BOOST_STRINGIZE(BUILDID)))

#define APPNAME "ApexDC++"
#define VERSIONSTRING "1.5.10"
#define DCVERSIONSTRING "0.785"
#define SDCVERSIONSTRING "2.43"
#define BUILDID $WCREV$

// - Added these here so Lee can update them without me, Crise
#define HOMEPAGE "http://www.apexdc.net/"
#define DISCUSS "http://forums.apexdc.net/"
#define DONATE "http://www.apexdc.net/donate/"
#define GUIDES "http://forums.apexdc.net/index.php?showforum=13"

#define GEOIPFILE "http://www.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip"

#define COMPLETEVERSIONSTRING(user)	APPNAME " " VERSIONSTRING SVNVERSION + (!user.empty() ?  " [Login: " + user + "] " : " ") + "(" CONFIGURATION_TYPE ")"

#ifdef _WIN64
# define CONFIGURATION_TYPE "x86-64"
# define PDB_NAME "ApexDC-x64.pdb"
#else
# define CONFIGURATION_TYPE "x86-32"
# define PDB_NAME "ApexDC.pdb"
#endif

#if !BETA
# define VERSION_URL "http://update.apexdc.net/version.xml"
# define SVNVERSION "$WCMODS?m:$"
#else
# define VERSION_URL "http://update.apexdc.net/beta.xml"
# define SVNVERSION ".$WCREV$$WCMODS?m:$"
#endif

#ifdef NDEBUG
# define INST_NAME "{APEXDC-AEE8350A-B49A-4753-AB4B-E55479A48351}"
#else
# define INST_NAME "{APEXDC-AEE8350A-B49A-4753-AB4B-E55479A48350}"
#endif

/* Update the .rc file as well... */

/**
 * @file
 * $Id: version-template.h 2026 2014-01-14 18:48:02Z crise $
 */
